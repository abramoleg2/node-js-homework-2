const express = require('express');
const authRouter = express.Router();
const authController = require('../controllers/auth-controller');

// middleware that is specific to this router
// usersRouter.use(function timeLog(req, res, next) {
//     console.log('Time: ', Date.now());
//     next();
// });
authRouter.post('/register', authController.registration);
authRouter.post('/login', authController.login);

module.exports = authRouter;
