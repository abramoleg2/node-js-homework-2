const express = require('express');
const usersRouter = express.Router();
const usersController = require('../controllers/users-controller');

// middleware that is specific to this router
// usersRouter.use(function timeLog(req, res, next) {
//     console.log('Time: ', Date.now());
//     next();
// });
usersRouter.get('/me', usersController.profileInfo);
usersRouter.delete('/me', usersController.deleteProfile);
usersRouter.patch('/me', usersController.changePassword);

module.exports = usersRouter;
