const express = require('express');
const notesRouter = express.Router();
const notesController = require('../controllers/notes-controller');

// middleware that is specific to this router
// usersRouter.use(function timeLog(req, res, next) {
//     console.log('Time: ', Date.now());
//     next();
// });
notesRouter.get('/', notesController.getUserNotes);
notesRouter.post('/', notesController.addUserNote);
notesRouter.get('/:id', notesController.getNoteById);
notesRouter.put('/:id', notesController.updateNoteById);
notesRouter.patch('/:id', notesController.checkNoteById);
notesRouter.delete('/:id', notesController.deleteNoteById);

module.exports = notesRouter;
