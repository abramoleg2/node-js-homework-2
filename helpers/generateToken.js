const jwt = require('jsonwebtoken');

function generateToken(userData) {
  const payload = {
    'id': userData._id,
    'username': userData.username,
    'password': userData.password,
  };
  return jwt.sign(payload, process.env.SECRET, {expiresIn: '30d'});
}

module.exports = generateToken;
