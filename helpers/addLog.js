const fs = require('fs');
const path = require('path');

function addToLog(request, response) {
  const logPath = path.join(__dirname, '../', 'logs.txt');
  fs.appendFile(
      logPath, `{requestUrl:${request.url},
              requestMethod:${request.method},
              responseStatus: ${response.status}}\n`, (err) => {
        if (err) {
          /* eslint-disable */
      console.log("Failed to write request parameters to log");
      /* eslint-enable */
        }
      });
}

module.exports = addToLog;
