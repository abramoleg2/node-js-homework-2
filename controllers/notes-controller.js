const Note = require('../models/note');
const jwt = require('jsonwebtoken');
const addToLog = require('../helpers/addLog.js');

class NotesOptions {
  async getUserNotes(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        const authorId = decoded.id;
        const offset = +req.query.offset || 0;
        const limit = +req.query.limit || 0;
        const userNotes = await Note.find({author: authorId})
            .skip(offset)
            .limit(limit);
        res.status(200).json({
          offset: offset,
          limit: limit,
          count: userNotes.length,
          notes: userNotes,
        });
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }

  async addUserNote(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      const text = req.body.text;
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        const authorId = decoded.id;
        const newNote = new Note({authorId: authorId, text: text});
        await newNote.save();
        res.status(200).json({message: 'Success'});
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }

  async getNoteById(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      const noteId = req.params.id;
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        const userNote = await Note.findOne({
          $and: [{_id: noteId}, {authorId: decoded.id}],
        });
        res.status(200).json({
          note: userNote,
        });
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }

  async updateNoteById(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      const noteId = req.params.id;
      const text = req.body.text;
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        await Note.replaceOne({
          $and: [{_id: noteId}, {authorId: decoded.id}],
        }, {
          authorId: decoded.id, text: text,
        });
        res.status(200).json({message: 'Success'});
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }

  async checkNoteById(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      const noteId = req.params.id;
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        const userNote = await Note.findOne({
          $and: [{_id: noteId}, {authorId: decoded.id}],
        });
        const noteStatus = userNote.completed;
        await Note.updateOne({
          $and: [{_id: noteId}, {authorId: decoded.id}],
        }, {
          $set: {completed: !noteStatus},
        });
        res.status(200).json({message: 'Success'});
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }

  async deleteNoteById(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      const noteId = req.params.id;
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        await Note.deleteOne({$and: [{_id: noteId}, {authorId: decoded.id}]});
        res.status(200).json({message: 'Success'});
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }
}

module.exports = new NotesOptions();
