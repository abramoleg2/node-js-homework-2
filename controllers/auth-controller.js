const User = require('../models/user');
const cryptPassword = require('../helpers/cryptPassword');
const generateToken = require('../helpers/generateToken');
const addToLog = require('../helpers/addLog.js');

class Authentification {
  async registration(req, res) {
    try {
      const username = req.body.username;
      const password = req.body.password;
      const newRegisterUser = await User.findOne({username: username});
      if (newRegisterUser) {
        addToLog(req, {status: '400'});
        return res.status(400).json({message: 'Bad request'});
      }
      const createHashPassword = new Promise((resolve, reject) => {
        resolve(cryptPassword.hashPassword(password));
      });
      const hashPassword = await createHashPassword;
      const newUser = new User({username: username, password: hashPassword});
      await newUser.save();
      res.status(200).json({message: 'Success'});
      addToLog(req, {status: '200'});
    } catch (err) {
      res.status(500).json({message: 'Internal server error'});
      addToLog(req, {status: '500'});
    }
  }

  async login(req, res) {
    try {
      const username = req.body.username;
      const password = req.body.password;
      const newLoginUser = await User.findOne({username: username});
      if (!newLoginUser) {
        addToLog(req, {status: '400'});
        return res.status(400).json({message: 'Bad request. User not found'});
      }
      const comparePassword = new Promise((resolve, reject) => {
        resolve(cryptPassword.comparePassword(password, newLoginUser.password));
      });
      const compareResult = await comparePassword;
      if (!compareResult) {
        addToLog(req, {status: '400'});
        return res.status(400).json({
          message: 'Bad request. Password is wrong',
        });
      }
      const token = generateToken(newLoginUser);
      res.status(200).json({message: 'Success', jwt_token: `${token}`});
      addToLog(req, {status: '200'});
    } catch (err) {
      res.status(500).json({message: 'Internal server error'});
      addToLog(req, {status: '500'});
    }
  }
}

module.exports = new Authentification();
