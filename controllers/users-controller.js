const jwt = require('jsonwebtoken');
const User = require('../models/user');
const cryptPassword = require('../helpers/cryptPassword');
const addToLog = require('../helpers/addLog.js');

class ProfileOptions {
  async profileInfo(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        const findUser = await User.findOne({username: decoded.username});
        res.status(200).json({
          user: {
            '_id': findUser._id,
            'username': findUser.username,
            'createdDate': findUser.created,
          },
        });
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }

  async deleteProfile(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        const deleteUser = await User.findOne({username: decoded.username});
        if (!deleteUser) {
          addToLog(req, {status: '400'});
          return res.status(400).json({message: 'Bad request. User not found'});
        }
        await User.deleteOne({username: decoded.username});
        res.status(200).json({message: 'Success'});
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }

  async changePassword(req, res) {
    try {
      const reqToken = req.headers.authorization.split(' ')[1];
      const oldPassword = req.body.oldPassword;
      const newPassword = req.body.newPassword;
      try {
        const decoded = jwt.verify(reqToken, process.env.SECRET);
        const needUser = await User.findOne({username: decoded.username});
        if (!needUser) {
          addToLog(req, {status: '400'});
          return res.status(400).json({message: 'Bad request. User not found'});
        }
        const comparePassword = new Promise((resolve, reject) => {
          resolve(
              cryptPassword.comparePassword(oldPassword, needUser.password),
          );
        });
        const compareResult = await comparePassword;
        if (!compareResult) {
          addToLog(req, {status: '400'});
          return res.status(400).json({
            message: 'Bad request. Old password is wrong',
          });
        }
        if (oldPassword === newPassword) {
          addToLog(req, {status: '400'});
          return res.status(400).json({
            message: 'Bad request. Password is the same',
          });
        }
        const createHashPassword = new Promise((resolve, reject) => {
          resolve(cryptPassword.hashPassword(newPassword));
        });
        const hashPassword = await createHashPassword;
        await User.updateOne({
          username: decoded.username,
        }, {
          $set: {password: hashPassword},
        });
        res.status(200).json({message: 'Success'});
        addToLog(req, {status: '200'});
      } catch (err) {
        res.status(500).json({message: 'Internal server error'});
        addToLog(req, {status: '500'});
      }
    } catch (err) {
      res.status(400).json({message: 'Bad request'});
      addToLog(req, {status: '400'});
    }
  }
}

module.exports = new ProfileOptions();
