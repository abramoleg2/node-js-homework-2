const express = require('express');
const myApp = express();
const fs = require('fs');
const usersRouter = require('./routers/users-routes');
const notesRouter = require('./routers/notes-routes');
const authRouter = require('./routers/auth-routes');
require('./database');

if (process.env.NODE_ENV) {
  require('dotenv').config({path: `${__dirname}/.env.${process.env.NODE_ENV}`});
} else {
  require('dotenv').config();
};

try {
  const logFile = `${__dirname}/logs.txt`;
  if (!fs.existsSync(logFile)) {
    fs.writeFileSync(logFile, '');
  };
} catch (err) {
  throw err;
}

myApp.use(express.json());
myApp.use('/api/users', usersRouter);
myApp.use('/api/notes', notesRouter);
myApp.use('/api/auth', authRouter);


const PORT = process.env.PORT || 8080;
myApp.listen(PORT, () => {
  /* eslint-disable */
  console.log(`Serever 🚀 on port ${PORT}...`);
  /* eslint-enable */
});
