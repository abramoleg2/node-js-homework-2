const {Schema, model} = require('mongoose');

const Note = new Schema({
  authorId: {type: String, required: true},
  completed: {type: Boolean, default: false},
  text: {type: String, required: true},
  created: {type: Date, default: Date.now},
});

module.exports = model('Note', Note);
