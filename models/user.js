const {Schema, model} = require('mongoose');

const User = new Schema({
  username: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  created: {type: Date, default: Date.now},
});

module.exports = model('User', User);
