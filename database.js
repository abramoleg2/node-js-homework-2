const mongoose = require('mongoose');
const MONGO_USER = 'testUser';
const MONGO_PASSWORD = 'test';
const MONGO_NAME_DB = 'test';

const url = `mongodb+srv://${MONGO_USER}:${MONGO_PASSWORD}@cluster.kmmr3.mongodb.net/${MONGO_NAME_DB}?retryWrites=true&w=majority`;
try {
  mongoose.connect(url, {useNewUrlParser: true});
} catch (err) {
  /* eslint-disable */
    console.log("My error:", err);
    /* eslint-enable */
}

mongoose.connection.on('connected', () => {
  /* eslint-disable */
    console.log("connected to Mongo DB");
    /* eslint-enable */
});
